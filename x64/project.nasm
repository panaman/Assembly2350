;Assembly program that calculates a users choice of either combination or permutation.
; -Your main procedure will print a menu for the user and read their input.
;      factorial, combination and permutation.
; -Factorial should be a recursively defined procedure that takes an integer
;      argument and returns its factorial.
; -The combination and permutation procedures will use the factorial procedure
;      that you create in order to perform their calculations.
; -After each selection the user should be asked if they would like to perform
;      another operation.
; -If they enter "yes" (not just 'y') they should be able to continue.
; -Otherwise, the program should exit

extern puts
extern printf
extern scanf

SECTION .data
	menu:      db  "0: Factorial", 10, "1: Combonation", 10, "2: Permutation", 10, "Choice: ", 0
	promptN:   db  "Enter N value: ", 0
	promptR:   db  "Enter R value: ", 0
	promptNum: db  "Enter number: ", 0
	result:    db  "Result: %ld", 10, 0
	playagain: db  "Play again: ", 0
	yesString: db  "yes", 0
	again:     db  "   ", 0
	numfmt:    db  "%d", 0
	stringfmt: db  "%s", 0
	N:         dq  0
	R:         dq  0
	num:       dq  0
	choice:    db  0

SECTION .text
global main
main:
	push  rbp

	start:
	mov   rdi, menu
	mov   rsi, 0
	xor   rax, rax
	call  printf

	mov   rdi, numfmt
	mov   rsi, choice
	xor   rax, rax
	call  scanf

	cmp   BYTE [choice], 1  ; Compare user input to decide what function to run
	je    combostart
	;jg    permstart

	factstart:              	; function for input and output of factorial
		mov   rdi, promptNum    ; move the prompt to RDI (1st parameter loc)
		call  puts              ; call extern puts proc
		
		mov   rdi, numfmt       ; move input format to rdi
		mov   rsi, num          ; move location of ans var to rsi
		mov   rax, 0
		call  scanf             ; call scanf

		mov   rdi, [num]
		call  fact              ; call factorial function

		mov   rsi, rax          ; set up print statement
		mov   rdi, result
		mov   rax, 0
		call  printf

		jmp   restart           ; jump to restart

	combostart:
		mov   rdi, promptN      ; move the prompt to RDI (1st parameter loc)
		mov   rsi, 0
		mov   rax, 0
		call  printf            ; call extern printf proc
		
		mov   rdi, numfmt       ; move input format to rdi
		mov   rsi, N            ; move location of N var to rsi
		mov   rax, 0
		call  scanf             ; call scanf

		mov   rdi, promptR      ; move the prompt to RDI (1st parameter loc)
		mov   rsi, 0
		mov   rax, 0
		call  printf            ; call extern printf proc
		
		mov   rdi, numfmt       ; move input format to rdi
		mov   rsi, R            ; move location of R var to rsi
		mov   rax, 0
		call  scanf             ; call scanf

	restart:
		mov   rdi, playagain     ; prompt user if they want to play again
		mov   rax, 0
		call  printf

		mov   rdi, stringfmt     ; getting input
		mov   rsi, again
		mov   rax, 0
		call  scanf

		mov   rdi, yesString     ; compare string code
		mov   rsi, again
		mov   rcx, 4
		rep   cmpsb
		jne   out
		
	    stringsMatch:
			jmp start

    out:
		xor   rax, rax
		pop   rbp
		ret


fact:
	push  rbp
	mov   rbp, rsp

	cmp   rdi, 2
	jl    base
	jmp   cont

	base:
		mov   rax, 1
		jmp   endfact

	cont:
		push  rdi
		dec   rdi
		call  fact
		pop   rdi
		mul   rdi

	endfact:
		mov   rsp, rbp
		pop   rbp
		ret


; rdi = N
; rsi = R
combo:
	push  rbp
	mov   rbp, rsp        ; Stack frame

	call  fact
	mov   r8, rax    ; r8 = N!

	mov   rdi, rsi
	call  fact
	mov   r9, rax    ; r9 = R!


	mov   rsp, rbp
	pop   rbp
	ret	